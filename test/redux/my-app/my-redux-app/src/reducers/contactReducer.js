
import {GET_CONTACTS,DELETE_CONTACT,ADD_CONTACT,TOTAL} from '../actions/types'



const initialState={
    
    mobilni: [
        {
          id: 1,
          name: 'IPHONE 7',
          price:700,
          img:"mobile2"
        },
        {
          id: 2,
          name: 'SAMSUNG S10',
          price:1000,
          img:"mobile3"
          
        },
        {
          id: 3,
          name: 'HUAWEI P30 PRO',
          price:1100,
          img:"mobile1"
          
        }
      ],
      televizori:[
        {
        id: 1,
        name: "PANASONIC",
        price:400,
        img:"tv1"
        },
        {
          id: 2,
          name: "LG",
          price:700,
          img:"tv2"
        },
        {
          id: 3,
          name: "SAMSUNG",
          price:650,
          img:"tv3"
        }
      ],
  
      cart:[],
      total:0
      
}

export default function(state=initialState,action){

    switch(action.type){

        case GET_CONTACTS:
        return {
            ...state
        } 
        case DELETE_CONTACT:
          return {
              ...state,
              cart:state.cart.filter(cart =>
                cart.id!==action.payload
                
                )

          }
          case ADD_CONTACT:
            return {
                ...state,
                cart:[action.payload,...state.cart]
            }
            case TOTAL :
      
            return {
              ...state,
               total:action.payload.reduce( (acc, obj) => acc + obj.price , 0) 
            }

        default:
            return state;
    }
}