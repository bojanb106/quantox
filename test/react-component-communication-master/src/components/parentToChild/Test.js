import React, { Component } from 'react'

import {increment,decrement,test,add ,del,totals} from "../../actions/index"
import {connect} from "react-redux"

export class Test extends Component {

    state ={
 
    }

inc = () =>{
this.props.increment()
this.props.test()

}

dec = () =>{
    this.props.decrement()
    this.props.test()

}

add = (name) =>{

    this.props.totals(this.props.listOfNames)
this.props.add(name)
// console.log(name)



}
    render() {
      const {counter,text,names} =this.props
        return (
            <div>

            <h1>{counter}</h1>
            <h2>{text}</h2>
            <button onClick={this.inc}>+</button>
            
            <button onClick={this.dec}>-</button>
            <h1>LIST OF NAMES</h1>
            {names.map((n)=><ul><li>{n.name}</li><button onClick={this.add.bind(this,n)}>add name to CHOSEN list</button></ul>)}
            </div>
        )
    }
}


const mapStateToProps=(state)=>({
   
    counter:state.test.counter,
    text:state.test.text,
    names:state.test.names,
    listOfNames:state.test.listOfNames
    })
export default connect(mapStateToProps,{increment,decrement,test,add,del,totals
})(Test);