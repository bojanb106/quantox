function getData() {

    fetch("https://api.myjson.com/bins/a6da9").then(res => {
        res.json().then(data => {
            questionData = data;
            render(data)
        })
    })


}


getData()
// Data from API..




let randomQuestions;

function render(data) {

    let selected;


    // Get 5 random question from db..

    function getRandomQuestions() {


        const shuffled = data.sort(() => 0.5 - Math.random());

        // Get sub-array of first n elements after shuffled
        selected = shuffled.slice(0, 5);
        return selected;

    }
    console.log(data)


    randomQuestions = getRandomQuestions()

    populate()
    resetAllQuestions()
    firstQuestion()

}



// Function for populate questions and image..

function populate() {


    for (let i = 0; i < randomQuestions.length; i++) {

        // Set  corect answers
        answer = randomQuestions[i].continent;

        // Get 2 wrang answers without correct answer

        sliceCorrectAnswers = randomQuestion(randomQuestions[i].continent)



        // Two wrong answers
        wrongAnswers = getWrongAnswers(sliceCorrectAnswers)



        // List with two wrong answers nad correct answer;

        allQArray = [answer, ...wrongAnswers]


        // Shuffle the  all questions
        allQ = mixAllAnswers(allQArray)


        getAns(answer)


        pageWithQuestions(i, allQ, randomQuestions[i].image)


    }


}



// Hide all questions first...
function resetAllQuestions() {
    for (let i = 0; i < allWraperAnswer.length; i++) {
        allWraperAnswer[i].style.display = "none"

    }

}
resetAllQuestions()


// Set first question
function firstQuestion() {
    allWraperAnswer[0].style.display = "block"

}



// ?Html for image with answers...

function pageWithQuestions(i, answers, img) {


    questionsWraper = `
    
<div class="answerWraper" id="answerWraper">

<p id="qPara" class="allP">Question <span id="questionNumber">${i+1} od 5</span></p>
<img src=${img}>
<div class="questionButtons">
<button id="q1" onclick="checkAnswers(this)" class="answers">${answers[0]}</button>
<button id="q2" onclick="checkAnswers(this)" class="answers">${answers[1]}</button>
<button id="q3" onclick="checkAnswers(this)" class="answers">${answers[2]}</button>
</div>



<div class="btnWraper">
    <button class="btn" onClick="next()" id="next">Next</button>
</div>

</div>
</div>

`

    document.getElementById("questions").innerHTML += questionsWraper;
}

let l = document.querySelector(".questionButtons");
console.log(l)