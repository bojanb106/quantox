// Function for populate cores from local storage

function pupulateScores() {
    const sc = JSON.parse(localStorage.getItem("items"));
    if (sc === null) {
        document.getElementById("scores").innerHTML = "";
    } else {
        let sortScores = sc.sort((a, b) => b - a);

        for (let i = 0; i < sortScores.length; i++) {
            if (i < 3) {
                scoresHtml = `<div>
              <div class="res-group">
              <div class="player">#${i + 1}</div>
              <div class="pts">${sortScores[i]} pts</div>
              </div>
  
              </div>
  
              `;

                scores.innerHTML += scoresHtml;
            }
        }
    }
}

pupulateScores()