import counterReducer from "./counter"

import isLogeIn from "./isLoged"

import { combineReducers } from "redux"

const allReducers=combineReducers({

    test:counterReducer,
    loged:isLogeIn
})

export default allReducers;