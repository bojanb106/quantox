import React, { Component } from 'react'
import {increment,decrement,test,del,totals } from "../../actions/index"
import {connect} from "react-redux"


export  class child extends Component {

    del = (id) =>{
   
this.props.del(id)
this.props.totals(this.props.listOfNames)

    }
   
    render() {
        const {listOfNames} =this.props 
        return (
            <div>
            <h1>CHOSEN LIST OF NAMES</h1>
                 {listOfNames.map(list=><ul><li>{list.name}</li><button onClick={this.del.bind(this,list.id)}>Remove from list</button></ul>)}
            </div>
        )
    }
}

const mapStateToProps=(state)=>({
   
    listOfNames:state.test.listOfNames
    })
export default connect(mapStateToProps,{increment,decrement,test,del,totals
})(child);