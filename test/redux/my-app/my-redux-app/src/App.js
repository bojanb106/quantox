import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import Contacts from './components/contacts/Contacts';
import Mobilni from "./components/contacts/Mobilni"
import EditContact from './components/contacts/EditContact';
import Header from './components/layout/Header';
import Televizori from "./components/contacts/Televizori"
import Cart from "./components/contacts/Cart"

import NotFound from './components/pages/NotFound';
import {Provider} from "react-redux"
import store from "./store"

import "bootstrap/dist/css/bootstrap.min.css"
import './App.css';

class App extends Component {
  render() {
    console.log(store)
    return (
      <Provider store={store}>
      <Router>
        <div className="App">
          <Header branding="Web Shop" />
          <div className="container">
            <Switch>
              <Route exact path="/" component={Contacts} />
              <Route exact path="/contact/mobilni" component={Mobilni} />
              <Route exact path="/contact/edit/:id" component={EditContact} />
              <Route exact path="/contact/televizori" component={Televizori} />
              <Route exact path="/contact/cart" component={Cart} />
              <Route component={NotFound} />
            </Switch>
          </div>
        </div>
      </Router>
      </Provider>
    );
  }
}



export default App;
