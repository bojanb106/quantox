function getData() {


    // let response = await fetch("https://api.myjson.com/bins/a6da9")
    // let datas = await response.json();
    // console.log(datas)
    // return datas;
    fetch("https://api.myjson.com/bins/a6da9").then(res => {
        res.json().then(data => {
            questionData = data;
            render(data)
        })
    })


}


getData()






// Init a variables for app....

let questions = document.getElementById("questions")
let allAnswers = document.getElementsByClassName("answers")
let questionNumber = document.getElementById("questionNumber");
let qPara = document.getElementById("qPara")
let allWraperAnswer = document.getElementsByClassName("answerWraper")


let wrongAnswers;

let answer;

let img;

let questionsWraper;

let continentsWithOutCorrect = []

let sliceCorrectAnswers;

let allQArray = []

let allQ;

let obj = []

let count = 1;

let score = 0;

let allScores;

let scoreP;

let scoresHtml;

let iter = 0;
let arr = []







// List of continents...
let continents = ["Africa", "Europe", "Asia", "Oceania", "Antarctica", "North America", "South America"]



// Data from API..


// let data = [{
//         answer: "Antarctica",
//         img: "https://i.imgur.com/BlrmuXu.jpg"


//     },
//     {
//         answer: "Antarctica",
//         img: "https://i.imgur.com/E7n3P77.jpg"
//     },
//     {
//         answer: "Europe",
//         img: "https://i.imgur.com/nooVXqp.jpg"


//     },
//     {

//         answer: "North America",
//         img: "https://i.imgur.com/BWwPYCI.jpg"


//     },
//     {
//         answer: "Africa",
//         img: "https://i.imgur.com/QBo4Huk.jpg"


//     },
//     {
//         answer: "Asia",
//         img: "https://i.imgur.com/RVowloS.jpg"

//     },


// ]

let randomQuestions;

function render(data) {





    let selected;


    // Get 5 ranom question from db..

    function getRandomQuestions() {
        console.log(data)

        const shuffled = data.sort(() => 0.5 - Math.random());

        // Get sub-array of first n elements after shuffled
        selected = shuffled.slice(0, 5);
        return selected;

    }



    randomQuestions = getRandomQuestions()



}






function populate() {

    console.log(randomQuestions)
    for (let i = 0; i < randomQuestions.length; i++) {

        // Set  corect answers
        answer = randomQuestions[i].continent;

        // Get 2 wrang answers without correct answer

        sliceCorrectAnswers = randomQuestion(randomQuestions[i].continent)




        wrongAnswers = getWrongAnswers(sliceCorrectAnswers)



        allQArray = [answer, ...wrongAnswers]

        allQ = mixAllQuestions(allQArray)
        console.log(allQ)

        getAns(answer)


        pageWithQuestions(i, allQ, randomQuestions[i].image)


    }


}

populate()

// Hide all questions first...
function resetAllQuestions() {
    for (let i = 0; i < allWraperAnswer.length; i++) {
        allWraperAnswer[i].style.display = "none"

    }

}


// Set first question
function firstQuestion() {
    allWraperAnswer[0].style.display = "block"

}
// firstQuestion()




// ?Html for image with answers...

function pageWithQuestions(i, answers, img) {


    questionsWraper = `
    
<div class="answerWraper" id="answerWraper">
<h4 class="quizHeading">CONTINENT QUIZ</h4>
<p id="qPara" class="allP">Question <span id="questionNumber">${i+1} od 5</span></p>
<img src=${img}>
<button id="q1" onclick="checkAnswers(this)" class="answers">${answers[0]}</button>
<button id="q2" onclick="checkAnswers(this)" class="answers">${answers[1]}</button>
<button id="q3" onclick="checkAnswers(this)" class="answers">${answers[2]}</button>



<div class="btnWraper">
    <button class="btn" onClick="next()" id="next">Next</button>
</div>

</div>
</div>

`

    document.getElementById("questions").innerHTML += questionsWraper;
}



// // Filter out correct answer from two random wrong answers..
function randomQuestion(answer) {

    continentsWithOutCorrect = continents.filter(res => res !== answer)

    return continentsWithOutCorrect

}

// 
function getWrongAnswers(r) {

    const shuffled = r.sort(() => 0.5 - Math.random());


    let selected = shuffled.slice(0, 2);

    return selected;
}


function mixAllQuestions(a) {

    const shuffled = a.sort(() => 0.5 - Math.random());


    let selected = shuffled.slice(0, 3);


    return selected;
}

// Push all answers to arry

function getAns(arrs) {
    arr.push(arrs)


}

// // Next button

function next() {


    resetAllQuestions()

    iter++



    if (iter > 4) {

        document.getElementById("finish").style.display = "block"
        resetAllQuestions()
        document.getElementById("questions").style.display = "none"


        iter = 0;



        document.getElementById("res").textContent = score;

        if (localStorage.getItem("items") === null) {
            allScores = [];
        } else {
            allScores = JSON.parse(localStorage.getItem("items"));
        }

        allScores.push(score);

        localStorage.setItem("items", JSON.stringify(allScores));
    }

    for (let i = 0; i < allWraperAnswer.length; i++) {
        allWraperAnswer[iter].style.display = "block"

    }

    for (let i = 0; i < allAnswers.length; i++) {

        allAnswers[i].disabled = false;
        allAnswers[i].style.background = "white";
    }


}


// Check answers

function checkAnswers(e) {


    // Here we used arr variable to check answers...

    if (e.textContent === arr[iter]) {
        score += 750;

    } else {
        score += 0;
    }
    for (let i = 0; i < allAnswers.length; i++) {
        allAnswers[i].disabled = true;
        allAnswers[i].style.background = "white";

        if (allAnswers[i].textContent === arr[iter]) {
            allAnswers[i].style.background = "green";
        }

        if (
            allAnswers[i].textContent !== arr[iter] &&
            e.textContent === allAnswers[i].textContent
        ) {
            allAnswers[i].style.background = "red";

        }
    }

}


function finished() {

    document.getElementById("btnWraper").style.display = "block"
    document.getElementById("finish").style.display = "none"
    scores.innerHTML = ""

    score = 0;
    iter = 0;
    pupulateScores();





}



// Populate scores

function pupulateScores() {
    const sc = JSON.parse(localStorage.getItem("items"));
    if (sc === null) {
        document.getElementById("scores").innerHTML = "";
    } else {
        let sortScores = sc.sort((a, b) => b - a);

        for (let i = 0; i < sortScores.length; i++) {
            if (i < 3) {
                scoresHtml = `<div>
              <div class="res-group">
              <div class="player">#${i + 1}</div>
              <div class="pts">${sortScores[i]} pts</div>
              </div>
  
              </div>
  
              `;

                scores.innerHTML += scoresHtml;
            }
        }
    }
}





function reset() {


    // document.body.innerHTML = html;




    resetAllQuestions()
    firstQuestion()


    // document.getElementById("btnWraper").style.display = "none"

    // document.getElementById("questions").style.display = "block";
    // document.getElementById("home").style.display = "none"







    // console.log(iter)
    // htmls(iter, radnom3, datas[iter].img)


}

function backToHome() {
    document.getElementById("home").style.display = "block";
    document.getElementById("btnWraper").style.display = "none"
}






let x = [1, 2, 3]

function getR() {

    const shuffled = x.sort(() => 0.5 - Math.random());

    // Get sub-array of first n elements after shuffled
    selected = shuffled.slice(0, 3);



    return selected;
}


console.log(getR())