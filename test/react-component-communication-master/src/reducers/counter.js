import uuid from "uuid"

 const initialState={
         counter:0,
         text:"manji od 3",
         names:[
             {
                 name:"Bojan",
                 age:20,
                 id:uuid()
             },
             {
                 name:"Stefan",
                 id:uuid(),
                 age:30
             }
         ],

         listOfNames:[{
             name:"Janko",
             age:40,
             id:uuid()
         }],
        total:0


}


const counterReducer = (state=initialState,action)=>{

   

    switch (action.type){
    
        case "INC":
            return {
                ...state,
                counter:state.counter+1
            }
    
            case "DEC" :
            
                    return{
                        ...state,
                        counter:state.counter-1
                    } 

                    case "TEST" :
            
                           return {
                               ...state,
                               text:state.counter>3 ? "veci od 3" : "manji od 3"

                           }

                     case "ADD" :
                         return{
                             ...state,
                            listOfNames:[...state.listOfNames,action.payload]
                         }   
                         
                         case "DEL" :
                                return{
                                    ...state,
                                   listOfNames:state.listOfNames.filter(list =>
                                       list.id!==action.payload
                                   )
                                } 


                                case "TOTAL" :
      
                                   return {
                                     ...state,
                                        total:action.payload.reduce( (acc, obj) => acc + obj.age , 0) 
                                                                                                            }
                    

            default:
                    return state;
    }
   
    
    }
    
    export default counterReducer;