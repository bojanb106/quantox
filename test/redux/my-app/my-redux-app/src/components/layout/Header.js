import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {connect} from "react-redux"
import { stat } from 'fs';

const Header = props => {
  const { branding , total,cart} = props;


 

  return (
 
    <nav className="navbar navbar-expand-sm navbar-dark bg-danger mb-3 py-0">
      <div className="container">
        <a href="/" className="navbar-brand">
          {branding}
        </a>
        <div>
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to="/" className="nav-link">
                <i className="fas fa-home" />Home
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/contact/mobilni" className="nav-link">
                <i className="fas fa-plus" /> Mobilni
  
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/contact/televizori" className="nav-link">
                <i className="fas fa-question" /> Televizori
              </Link>
            </li>
            <li className="nav-item">
            <Link to="/contact/cart" className="nav-link">
              <i className="fas fa-question" /> Cart
            
            </Link>
          </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

Header.defaultProps = {
  branding: 'My App'
};

Header.propTypes = {
  branding: PropTypes.string.isRequired
};

const mapStateToProps=(state)=>({
  cart:state.items.cart,
  total:state.items.total

  })
export default connect(mapStateToProps,{
})(Header);
