import React, { Component } from 'react'
import {totals} from "../../actions/index"
import {connect} from "react-redux"

export  class total extends Component {

state={
    total:this.props.total
}

    componentWillMount(){
       

        this.props.totals(this.props.listOfNames)
        
    }
   
    render() {
        const {total,listOfNames,totals} =this.props
        console.log(listOfNames)
        return (
            <div>
                <h1>Total : {this.props.total}</h1>
            </div>
        )
    }
}


const mapStateToProps=(state)=>({
     
    total:state.test.total,
    listOfNames:state.test.listOfNames
    })
export default connect(mapStateToProps,{totals
})(total);