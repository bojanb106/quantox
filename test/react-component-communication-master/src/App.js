import React, { Component } from 'react';
import './App.css';
import Test from './components/parentToChild/Test'
import Child from './components/parentToChild/Child'
import Total from "./components/parentToChild/total"

class App extends Component {
  state = {
    title:'placeholder title'
  }

  changeTheWorld = (newTitle) => {
      this.setState({title:newTitle});
  }

  render() {
    return (
      <div className="App">
        <Test/>
        <Total/>
        <Child/>
      </div>
    );
  }
}

export default App;
