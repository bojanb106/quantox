export const increment = () =>{

    return {
        type:"INC",
       
    };
}


export const decrement = () =>{

    return {
        type:"DEC",
        
    };
}


export const test = () =>{

    return {
        type:"TEST",
        
    };
}
export const add = (name) =>{

    return {
        type:"ADD",
        payload:name
        
    };
}
export const del = (id) =>{

    return {
        type:"DEL",
        payload:id
        
    };
    
}
export const totals = (names) =>  dispatch => {

    dispatch({
        type:"TOTAL",
        payload:names
        
    });
    
}