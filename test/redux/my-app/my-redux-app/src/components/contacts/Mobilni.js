import React, { Component } from 'react';
import {connect} from "react-redux"
import uuid from "uuid"
import {addContact}  from "../../actions/contactActions"


class Items extends Component {
 
  addToCart = (mob) =>{
    const item={
 id:uuid(),
 name:mob.name,
 price:mob.price,
 img:mob.img
}
 this.props.addContact(item)
}


  render() {

    const { mobilni } = this.props
  
    return (


    <React.Fragment>
  
      {mobilni.map(mob =>   <ul key={mob.id}><li>{mob.name}</li>  <li>{mob.price}</li> 
         
       
        <li><button onClick={this.addToCart.bind(this,mob)}>ADD TO CART</button></li>
      
      
       </ul>)}
       
      
 
    </React.Fragment>
  
    )
  }
}
Items.propTypes = {

};

const mapStateToProps=(state)=>({
  mobilni:state.items.mobilni
  })
  
  export default connect(mapStateToProps,
  {addContact}
  
   )(Items);
  

